package resizer

import (
	"gopkg.in/gographics/imagick.v2/imagick"
	"log"
	"net/http"
	"strconv"
	"fmt"
	"time"
	"github.com/spf13/viper"
	"resizer/getfile"
	"errors"
	"regexp"
	"math"
	"encoding/json"
	"github.com/cloudfoundry/gosigar"
	"runtime"
)

var debug bool

func init() {
	debug = false
	imagick.Initialize()
	//defer imagick.Terminate()
}


func Stat(w http.ResponseWriter, r *http.Request) {
	var err error

	uptime := sigar.Uptime{}
	uptime.Get()
	avg := sigar.LoadAverage{}
	avg.Get()

	type Message struct {
		Version string
		CPU int
		Total int64
		Http int64
		Now string
		ServerUptime string
		ServerLA string
	}

	m := Message{
		viper.GetString("info.version"),
		runtime.NumCPU(),
		getfile.StatTotal,
		getfile.StatHttp,
		time.Now().Format("15:04:05"),
		uptime.Format(),
		fmt.Sprintf("%s,%s,%s", strconv.FormatFloat(avg.One, 'f', 2, 64), strconv.FormatFloat(avg.Five, 'f', 2, 64), strconv.FormatFloat(avg.Fifteen, 'f', 2, 64))}

	json, err := json.Marshal(m)
	if err != nil {
		printError(w, r, err)
	}

	fmt.Fprintf(w, "%s", json)
}


// Resize image
func CropImage(w http.ResponseWriter, r *http.Request) {
	var err error
	returnStatus := 200

	r.ParseForm()  // parse arguments, you have to call this by yourself
	targetWidth := uint64(0)
	targetHeight := uint64(0)
	method := uint64(viper.GetInt("resizer.resampleMethod"))
	quality := uint64(viper.GetInt("resizer.quality"))
	cropW := uint64(0)
	cropH := uint64(0)
	cropX := uint64(0)
	cropY := uint64(0)
	nbrW := uint64(0)
	nbrH := uint64(0)
	nbrX := uint64(0)
	nbrY := uint64(0)

	if (r.FormValue("debug") != "") {
		debug = true
	}

	if debug {log.Printf("========================================\n")}

	if (r.FormValue("w") != "") {
		targetWidth, _ = strconv.ParseUint(r.FormValue("w"), 10, 0)
		if (targetWidth > uint64(viper.GetInt("resizer.maxWidth"))) {targetWidth = uint64(viper.GetInt("resizer.maxWidth"))}
	}
	if (r.FormValue("h") != "") {
		targetHeight, _ = strconv.ParseUint(r.FormValue("h"), 10, 0)
		if (targetHeight > uint64(viper.GetInt("resizer.maxHeight"))) {targetHeight = uint64(viper.GetInt("resizer.maxHeight"))}
	}

	if (r.FormValue("m") != "") {
		method, _ = strconv.ParseUint(r.FormValue("m"), 10, 0)
		if (method < 1 || method > 6) {method = uint64(viper.GetInt("resizer.resampleMethod"))}
	}
	if (r.FormValue("q") != "") {
		quality, _ = strconv.ParseUint(r.FormValue("q"), 10, 0)
		if (quality < 1 || quality > 100) {quality = uint64(viper.GetInt("resizer.quality"))}
	}
	if (r.FormValue("sw") != "") {
		cropW, _ = strconv.ParseUint(r.FormValue("sw"), 10, 0)
		if (cropW > uint64(viper.GetInt("resizer.maxWidth"))) {cropW = uint64(viper.GetInt("resizer.maxWidth"))}
	}
	if (r.FormValue("sh") != "") {
		cropH, _ = strconv.ParseUint(r.FormValue("sh"), 10, 0)
		if (cropH > uint64(viper.GetInt("resizer.maxHeight"))) {cropH = uint64(viper.GetInt("resizer.maxHeight"))}
	}
	if (r.FormValue("sx") != "") {
		cropX, _ = strconv.ParseUint(r.FormValue("sx"), 10, 0)
		if (cropX > uint64(viper.GetInt("resizer.maxX"))) {cropX = uint64(viper.GetInt("resizer.maxX"))}
	}
	if (r.FormValue("sy") != "") {
		cropY, _ = strconv.ParseUint(r.FormValue("sy"), 10, 0)
		if (cropY > uint64(viper.GetInt("resizer.maxY"))) {cropY = uint64(viper.GetInt("resizer.maxY"))}
	}
	if (r.FormValue("nw") != "") {
		nbrW, _ = strconv.ParseUint(r.FormValue("nw"), 10, 0)
	}
	if (r.FormValue("nh") != "") {
		nbrH, _ = strconv.ParseUint(r.FormValue("nh"), 10, 0)
	}
	if (r.FormValue("nx") != "") {
		nbrX, _ = strconv.ParseUint(r.FormValue("nx"), 10, 0)
	}
	if (r.FormValue("ny") != "") {
		nbrY, _ = strconv.ParseUint(r.FormValue("ny"), 10, 0)
	}

	if debug {log.Printf("Params: [cropW:%d, cropH:%d, cropX:%d, cropY:%d, targetWidth:%d, targetHeight:%d]\n", cropW, cropH, cropX, cropY, targetWidth, targetHeight)}

	inputImageUrl := r.FormValue("url")

	match, _ := regexp.MatchString(viper.GetString("protection.allowedOrigDomains"), inputImageUrl)
	if !match {
		printError(w, r, errors.New("Orig domain not allowed"))
		return
	}

	mw := imagick.NewMagickWand()
	defer mw.Destroy()

	start := time.Now()
	var imageBlob []byte
	err = getfile.LoadImage(inputImageUrl, &imageBlob)
	if err != nil {
		returnStatus = http.StatusNotFound
		err = getfile.LoadImage(viper.GetString("error.defaultOrig"), &imageBlob)
		cropW = 0
		cropH = 0
		if err != nil {
			printError(w, r, err)
			return
		}
	}
	err = mw.ReadImageBlob(imageBlob)
	if err != nil {
		printError(w, r, err)
		return
	}
	timeLoad := time.Now().Sub(start)
	start = time.Now()

	// orig coords
	origW := mw.GetImageWidth()
	origH := mw.GetImageHeight()

	// if target w or h was not set - set it proportionally
	if targetWidth == 0 || targetHeight == 0 {
		var ratio float64 = 0
		if cropH != 0 && cropW !=0 {
			ratio = float64(cropW) / float64(cropH)
		} else {
			ratio = float64(origW) / float64(origH)
		}
		if targetHeight == 0 {
			targetHeight = uint64(float64(targetWidth) / ratio)
		}
		if targetWidth == 0 {
			targetWidth = uint64(float64(targetHeight) * ratio)
		}
		if debug {log.Printf("Restored sizes: [w:%d,h:%d]\n", targetWidth, targetHeight)}
	}

	if targetWidth == 0 || targetHeight == 0 {
		printError(w, r, errors.New("Target Width or Height cant be restored from crop neither orig"))
		return
	}

	if (cropW ==0 || cropH == 0) {
		cropW, cropH, cropX, cropY = normalizeCropCoords(targetWidth, targetHeight, uint64(origW), uint64(origH))
	}

	if debug {log.Printf("step 1: [cropW:%d,cropH:%d,cropX:%d, cropY:%d]\n", cropW, cropH, cropX, cropY)}

	// crop image
	if (cropW > 0 && cropH > 0 && (uint(cropW) != uint(origW) || uint(cropH) != uint(origH)) ) {
		targetRatio := float64(targetWidth) / float64(targetHeight)
		cropRatio := float64(cropW) / float64(cropH)
		if debug {log.Printf("check ratios: [targetRatio:%s,cropRatio:%s]\n", strconv.FormatFloat(targetRatio, 'f', 2, 64), strconv.FormatFloat(cropRatio, 'f', 2, 64))}

		// target ratio and crop ratio are not the same? fix it
		if math.Abs(targetRatio - cropRatio) > viper.GetFloat64("resizer.ratioPrecize") {
			cropW2 := uint64(0)
			cropH2 := uint64(0)
			cropX2 := uint64(0)
			cropY2 := uint64(0)
			cropW2, cropH2, cropX2, cropY2 = normalizeCropCoords(targetWidth, targetHeight, uint64(cropW), uint64(cropH))
			if debug {log.Printf("step 2: [cropW2:%d,cropH2:%d,cropX2:%d, cropY2:%d]\n", cropW2, cropH2, cropX2, cropY2)}
			cropX = cropX + cropX2
			cropY = cropY + cropY2
			cropW = cropW2
			cropH = cropH2
		}

		// if we have a nonbreakable area - fix the coords
		if (nbrW > 0 && nbrH > 0 && (nbrX < cropX || nbrY < cropY || nbrX+nbrW > cropX+cropW || nbrY+nbrH > cropY+cropH) ) {
			if debug {log.Printf("step 3: crop area [w:%d,h:%d,x:%d,y:%d]\n", cropW, cropH, cropX, cropY)}
			if debug {log.Printf("step 3: non-breakable area [w:%d,h:%d,x:%d,y:%d]\n", nbrW, nbrH, nbrX, nbrY)}
			cropRatio = float64(cropW) / float64(cropH)
			if debug {log.Printf("step 3: Crop ratio %s\n", strconv.FormatFloat(cropRatio, 'f', 2, 64))}

			if cropRatio > 1 {
				if debug {log.Printf("step 3: fix Y\n")}
				cropY = nbrY
				if (cropY > uint64(origH) - uint64(cropH)) {cropY = uint64(origH) - uint64(cropH)}
			} else {
				if debug {log.Printf("step 3: fix X\n")}
				cropX = nbrX
				if (cropX > uint64(origW) - uint64(cropW)) {cropX = uint64(origW) - uint64(cropW)}
			}
		}

		err = mw.CropImage(uint(cropW), uint(cropH), int(cropX), int(cropY))
		if debug {log.Printf("Crop: [cropW:%d,cropH:%d,cropX:%d, cropY:%d]\n", cropW, cropH, cropX, cropY)}
		if err != nil {
			printError(w, r, err)
			return
		}
	}

	// additional crop in case target ratio is not equal to crop ratio
/*	targetRatio := float64(targetWidth) / float64(targetHeight)
	cropRatio := float64(cropW) / float64(cropH)
	if debug {log.Printf("check ratios: [targetRatio:%s,cropRatio:%s]\n", strconv.FormatFloat(targetRatio, 'f', 2, 64), strconv.FormatFloat(cropRatio, 'f', 2, 64))}
	if math.Abs(targetRatio - cropRatio) > viper.GetFloat64("resizer.ratioPrecize") {
		cropW, cropH, cropX, cropY = normalizeCropCoords(targetWidth, targetHeight, uint64(cropW), uint64(cropH))
		if debug {log.Printf("Additional crop: [cropW:%d,cropH:%d,cropX:%d, cropY:%d]\n", cropW, cropH, cropX, cropY)}
		err = mw.CropImage(uint(cropW), uint(cropH), int(cropX), int(cropY))
		if err != nil {
			printError(w, r, err)
			return
		}
	}*/
	timeCrop := time.Now().Sub(start)
	start = time.Now()
	if uint(cropW) != uint(targetWidth) || uint(cropH) != uint(targetHeight) {
		if debug {log.Printf("Resize: [targetWidth:%d,targetHeight:%d]\n", uint(targetWidth), uint(targetHeight))}
		switch (method) {
		case 1: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_POINT, 1)
		case 2: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_BOX, 1)
		case 3: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_TRIANGLE, 1)
		case 4: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_HERMITE, 1)
		case 5: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_HANNING, 1)
		case 6: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_HAMMING, 1)
		case 7: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_BLACKMAN, 1)
		case 8: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_GAUSSIAN, 1)
		case 9: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_QUADRATIC, 1)
		case 10: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_CUBIC, 1)
		case 11: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_CATROM, 1)
		case 12: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_MITCHELL, 1)
		case 13: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_JINC, 1)
		case 14: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_SINC, 1)
		case 15: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_SINC_FAST, 1)
		case 16: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_KAISER, 1)
		case 17: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_WELSH, 1)
		case 18: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_PARZEN, 1)
		case 19: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_BOHMAN, 1)
		case 20: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_BARTLETT, 1)
		case 21: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_LAGRANGE, 1)
		case 22: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_LANCZOS, 1)
		case 23: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_LANCZOS_SHARP, 1)
		case 24: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_LANCZOS2, 1)
		case 25: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_LANCZOS2_SHARP, 1)
		case 26: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_ROBIDOUX, 1)
		case 27: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_ROBIDOUX_SHARP, 1)
		case 28: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_COSINE, 1)
		case 29: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_SPLINE, 1)
		case 30: err = mw.ResizeImage(uint(targetWidth), uint(targetHeight), imagick.FILTER_SENTINEL, 1)
		}
		if err != nil {
			printError(w, r, err)
			return
		}
	}

	// remove all info from image except image itself
	err = mw.StripImage();
	if err != nil {
		printError(w, r, err)
		return
	}
	timeResize := time.Now().Sub(start)

	// set jpeg quality
	err = mw.SetImageCompressionQuality(uint(quality))
	if err != nil {
		printError(w, r, err)
		return
	}

	// set jpeg format
	err = mw.SetImageFormat("JPEG")
	if err != nil {
		printError(w, r, err)
		return
	}

	//get image data for echo
	blob := mw.GetImageBlob()

	w.Header().Set("Content-type", "image/jpeg")
	if returnStatus == 200 {
		w.Header().Set("X-Accel-Expires", viper.GetString("resizer.expire"))
	} else {
		w.WriteHeader(returnStatus)
	}
	fmt.Fprintf(w, "%s", blob)
	log.Printf("[load:%v,crop:%v,resize:%v, http:%d, total:%d]\n", timeLoad, timeCrop, timeResize, getfile.Stat("http"), getfile.Stat("total"))
}

func printError(w http.ResponseWriter, r *http.Request, err error) {
	//w.Header().Set("Location", "http://bm.img.com.ua/empty.gif")
	log.Printf("ERROR PROCESSING (%s): %s\n", err, r.URL)
	if viper.GetString("error.printErrorInHeader") != "" {
		w.Header().Set(viper.GetString("error.printErrorInHeader"), fmt.Sprintf("%s", err))
	}
	http.Redirect(w, r, viper.GetString("error.redirect"), 302)
}


func normalizeCropCoords(tw, th, ow, oh uint64) (w,h,x,y uint64) {
	oRatio := float64(ow) / float64(oh)
	tRatio := float64(tw) / float64(th)
	if (tRatio > oRatio) {
		ratio := tRatio/oRatio
		y = uint64((float64(oh) - float64(oh) / ratio) / 2);
		w = ow
		h = uint64(float64(oh) / ratio)
	} else {
		ratio := oRatio/tRatio
		x = uint64((float64(ow) - float64(ow) / ratio) / 2);
		h = oh
		w = uint64(float64(ow) / ratio)
	}
	return w,h,x,y
}
